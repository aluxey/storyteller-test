#!/usr/bin/env python3

CONNECTED = True
DISCONNECTED = False

CYCLON_FILE_EXPRESSION = "cyclon"
ST_FILE_EXPRESSION = "storyteller"

PARAMETERS_FN = "parameters.csv"
SEQUENCE_FN = "sequence.csv"
CHURN_FN = "churn.csv"

# Devices timeline
# Whole interval of a device's connection
TYPE_CONNECTED = 'connected'
# Interval when device is informed of the current session
TYPE_INFORMED = 'informed'
# Interval when device is being used
TYPE_BEING_USED = 'being_used'

ID_SIZE = 10
