#!/usr/bin/env python3

# The ChrunScheduler class pauses/unpauses Docker containers
# according to a Weibull distribution.


# import os
from . import docker_utils
import datetime
import numpy as np
import pandas as pd
import random
# import signal
# import sys
import threading
from concurrent.futures import ThreadPoolExecutor
import time


DISCONNECTED = 0
CONNECTED = 1

SCHEDULER_PERIOD = 0.01  # s

# Each device's average uptime varies uniformly
# from MIN_AVERAGE_UPTIME to MAX_AVERAGE_UPTIME
MIN_AVERAGE_UPTIME = 0.2
MAX_AVERAGE_UPTIME = 1

STATE_CHANGE_MIN_TIME = 0.5
STATE_CHANGE_DIST_SHAPE = 1.2
STATE_CHANGE_DIST_SCALE = 4


# ---------- Paused and unpaused distributions ----------


def get_timeout(average_uptime, state):
    delta = 0
    if state == DISCONNECTED:
        delta = STATE_CHANGE_MIN_TIME + \
            (1-average_uptime) * STATE_CHANGE_DIST_SCALE * \
            np.random.weibull(STATE_CHANGE_DIST_SHAPE)
    else:
        delta = STATE_CHANGE_MIN_TIME + \
            average_uptime * STATE_CHANGE_DIST_SCALE * \
            np.random.weibull(STATE_CHANGE_DIST_SHAPE)

    return datetime.datetime.utcnow() + \
        datetime.timedelta(seconds=delta)

# ---------- Scheduler and Device classes ----------


class DevicesScheduler:
    devices = []
    do_schedule = False
    n_devices = 0
    please_die = False
    thread = None

    def __init__(self, churn_parameters, devices_info, output_dir):

        self.n_devices = len(devices_info)

        if churn_parameters is not None and \
                churn_parameters['enable']:
            print("Scheduler is set for devices churn")
            self.do_schedule = True

            # if 'state_change_min_time' in churn_parameters['scheduler']:
            #     STATE_CHANGE_MIN_TIME = churn_parameters[
            #         'scheduler']['state_change_min_time']
            # if 'state_change_dist_scale' in churn_parameters['scheduler']:
            #     STATE_CHANGE_DIST_SCALE = churn_parameters[
            #         'scheduler']['state_change_dist_scale']
            # if 'min_average_uptime' in churn_parameters['scheduler']:
            #     MIN_AVERAGE_UPTIME = churn_parameters[
            #         'scheduler']['min_average_uptime']
            # if 'max_average_uptime' in churn_parameters['scheduler']:
            #     MAX_AVERAGE_UPTIME = churn_parameters[
            #         'scheduler']['max_average_uptime']

        for d in devices_info:
            average_uptime = 1
            if self.do_schedule:
                # Each device's average uptime varies
                # from MIN_AVERAGE_UPTIME to MAX_AVERAGE_UPTIME
                average_uptime = \
                    (MAX_AVERAGE_UPTIME - MIN_AVERAGE_UPTIME) * \
                    np.random.random() + MIN_AVERAGE_UPTIME

            self.devices.append(_Device(d, average_uptime, output_dir))

    def start(self):
        map(_Device.bootstrap, self.devices)

        if self.do_schedule:
            self.thread = threading.Thread(target=self.scheduler_loop)
            self.thread.start()

    def scheduler_loop(self):
        print("Scheduler loop started")
        while self.do_schedule:
            # Using "with", the exeuctor waits on its threads
            # to complete between each iteration
            with ThreadPoolExecutor() as e:
                e.map(_Device.change_state, self.devices)

                time.sleep(SCHEDULER_PERIOD)

    def use(self, device_id):
        self.devices[device_id].use()

    def kill(self):
        self.do_schedule = False
        if self.thread is not None:
            self.thread.join()
        # Stop the devices
        map(_Device.stop, self.devices)
        print("Successfully killed scheduler")


class _Device:
    average_uptime = None
    info = None
    lock = threading.Lock()
    state = None
    timeout = None
    df = None
    output_dir = ""

    def __init__(self, info, average_uptime, output_dir):
        self.average_uptime = average_uptime
        self.df = pd.DataFrame()
        self.output_dir = output_dir

        self.state = CONNECTED if \
            (random.random() <= average_uptime) else DISCONNECTED
        self.info = info

    def bootstrap(self):
        with self.lock:
            # Would introduce the bloated bootstrap code here: no can do
            # self._start()
            self.timeout = get_timeout(self.average_uptime, self.state)

            if self.state == DISCONNECTED:
                self._disconnect()

    def change_state(self):
        with self.lock:
            if self.timeout <= datetime.datetime.utcnow():
                self._do_change_state()

    def use(self):
        with self.lock:
            if self.state == DISCONNECTED:
                self._do_change_state()
            else:
                # Reinitialize the online time
                self.timeout = get_timeout(self.average_uptime, CONNECTED)

    def stop(self):
        docker_utils.stop_container(self.info['docker_id'])
        self.df.to_csv(self.output_dir+"churn_"+self.info['name']+".csv")

    def _do_change_state(self):
        if self.state == DISCONNECTED:
            self._connect()
            self.state = CONNECTED
            self.timeout = get_timeout(self.average_uptime, CONNECTED)
        else:
            self._disconnect()
            self.state = DISCONNECTED
            self.timeout = get_timeout(self.average_uptime, DISCONNECTED)
        self._write_changes()

    def _connect(self):
        print("{}: Unpausing container {}".format(
            datetime.datetime.utcnow(), self.info['name']))
        docker_utils.unpause_container(self.info['docker_id'])

    def _disconnect(self):
        print("{}: Pausing container {}".format(
            datetime.datetime.utcnow(), self.info['name']))
        docker_utils.pause_container(self.info['docker_id'])

    def _write_changes(self):
        self.df = self.df.append(pd.Series({
            'Timestamp': datetime.datetime.utcnow(),
            'Name': self.info['name'],
            'Address': self.info['ip'],
            'State': 'connected' if self.state == CONNECTED
            else 'disconnected',
        }), ignore_index=True)

# Trying to kill the other threads by catching interruptions


# Catch interrupt
# def sigint_handler(signal, frame):
#     global scheduler
#     if scheduler is not None:
#         print("Destroying the scheduler")
#         scheduler.kill()
#     sys.exit(0)


# signal.signal(signal.SIGINT, sigint_handler)
