#!/usr/bin/env python3

import numpy as np
import copy
import random
import matplotlib.pyplot as plt
#from matplotlib.ticker import ScalarFormatter
import networkx as nx
from networkx.drawing.nx_agraph import write_dot


DEVICES_NAMES = ['tablet', 'personal phone', 'laptop', 'computer',
                 'workstation', 'fridge', 'toaster', 'profesional phone',
                 "mom's laptop", "old tablet", "connected table", "connected cat"]
DEVICES_PROBA = [10, 20, 14, 6,
                 13, 3, 1, 10,
                 2, 1, 6, 2]


# ---------- Convenience getters ----------

def get_devices_names(n_devices):
    if n_devices <= 0:
        return []
    if n_devices <= len(DEVICES_NAMES):
        return copy.deepcopy(DEVICES_NAMES[:n_devices])
    else:
        devices_names = copy.deepcopy(DEVICES_NAMES)

        for m in range(2, 2+int(np.ceil(n_devices / len(DEVICES_NAMES)))):
            for i, name in enumerate(DEVICES_NAMES):
                if len(devices_names) < n_devices:
                    devices_names.append("{} {}".format(name, m))
                else:
                    return devices_names


def get_devices_proba(n_devices):
    devices_proba = []
    if n_devices <= 0:
        return []
    if n_devices <= len(DEVICES_PROBA):
        devices_proba = copy.deepcopy(DEVICES_PROBA[:n_devices])
    else:
        devices_proba = copy.deepcopy(DEVICES_PROBA)

        for _ in range(int(np.ceil(n_devices / len(DEVICES_NAMES)))):
            for i in range(len(DEVICES_PROBA)):
                if len(devices_proba) < n_devices:
                    devices_proba.append(DEVICES_PROBA[i])
                else:
                    break

            if len(devices_proba) >= n_devices:
                break

    total = np.sum(devices_proba)
    for i in range(len(devices_proba)):
        devices_proba[i] /= total

    return devices_proba


# ---------- Graph functions ----------

def is_connected(W):
    # Verify that every node has an outgoing probability
    for w in W:
        if np.sum(w) == 0:
            return False

    n_devices = len(W)
    G = nx.DiGraph()

    G.add_nodes_from(list(range(n_devices)))

    for i in range(n_devices):
        for j in range(n_devices):
            if W[i][j] == 0:
                continue
            G.add_edge(i, j)

    return nx.is_strongly_connected(G)


def random_walk(transition_matrix, length, initial_state=None, initial_proba=None):
    n_devices = len(transition_matrix)

    current = None
    if initial_state is not None:
        if initial_state >= n_devices:
            raise ValueError(
                "The initial_state is bigger than the size of the transition_matrix.")
        current = initial_state
    elif initial_proba is not None:
        if len(initial_proba) != n_devices:
            raise ValueError(
                "The size of the initial_proba is different from the size of the transition_matrix.")
        current = np.random.choice(a=range(n_devices), p=initial_proba)
    else:
        current = np.random.choice(a=range(n_devices))
        print("Random walk started at random node #{}.".format(current))

    S = [None]*length

    for i in range(length):
        S[i] = current
        # Next choices depend on the transition_matrix
        current = np.random.choice(
            a=range(n_devices), p=transition_matrix[current, :])

    return S


# ---------- Drawings ----------

def write_dot_markov_chain(W, fn="markov/mc.dot"):
    # Code taken here:
    # http://vknight.org/unpeudemath/code/2015/11/15/Visualising-markov-chains.html
    n_devices = len(W)
    devices_names = get_devices_names(n_devices)

    G = nx.MultiDiGraph(name="Alice's devices")
    for i, name in enumerate(devices_names):
        G.add_node(i, label=name)
    # G.add_nodes_from(DEVICES_NAMES)

    for i, origin_state in enumerate(devices_names):
        for j, destination_state in enumerate(devices_names):
            rate = W[i][j]
            if rate > 0:
                G.add_edge(i,
                           j,
                           weight=rate,
                           # For clarity when drawing edge labels
                           # decorate=True,
                           label="{:.0%}".format(rate))

    write_dot(G, fn)


def plot_model_probabilities(W, fn="plots/W_probabilities.svg", show=True, model_name=""):
    n_devices = len(W)
    xaxis = list(range(n_devices))

    n_cols = int(np.ceil(np.sqrt(n_devices)))
    n_rows = int(np.ceil(n_devices/n_cols))

    plt.clf()
    fig, axarr = plt.subplots(
        n_rows, n_cols, sharex=True, figsize=(3.5*n_cols, 1+2.2*n_rows))
    if model_name != "":
        fig.suptitle(
            "Device switching probabilities of model: {}".format(model_name))
    else:
        fig.suptitle("Device switching probabilities")
    fig.text(0.5, 0.04, 'ID of next device', ha='center')
    fig.text(0.06, 0.5, 'Probability', va='center', rotation='vertical')

    for i in range(n_devices):
        col = i % n_cols
        row = i // n_cols
        plot_single_model_probability(W, i, axarr[row, col])

        # axarr[row,col].plot(xaxis, w, 'ro', ms=12, mec='r')
        # axarr[row,col].vlines(xaxis, 0, w, colors='r', lw=4)
        # axarr[row,col].set_yscale('log')
        # axarr[row,col].set_xticks(np.arange(0, n_devices, np.floor((n_devices-1)/2)))
        # axarr[row,col].set_xticks(np.arange(0, n_devices), minor=True)
        # axarr[row,col].set_title("From device {}".format(i))

    plt.savefig(fn)
    if show:
        plt.show()


def plot_single_model_probability(W, device_id, ax):
    n_devices = len(W)
    xaxis = list(range(n_devices))
    w = W[device_id]
    # print(W)
    # print(w)
    # print()

    ax.plot(xaxis, w, 'ro', ms=12, mec='r')
    ax.vlines(xaxis, 0, w, colors='r', lw=4)
    ax.set_yscale('log')
    ax.set_xticks(np.arange(0, n_devices, np.floor((n_devices-1)/2)))
    ax.set_xticks(np.arange(0, n_devices), minor=True)

    #ax.set_title("From device {}".format(device_id))


# ---------- Distributions ----------

class zipf_gen:

    def __init__(self, N, s):
        self.N = N
        self.s = s
        self.harmonic = 1 / \
            np.sum(np.power(np.arange(1, self.N+1, dtype=float), -self.s))
        self._pmf = self.harmonic / np.arange(1, self.N+1) ** self.s

        if np.sum(self._pmf) != 1.:
            #print("PMF does not sum to 1 ({}).".format(np.sum(self._pmf)))
            self._pmf[0] += 1. - np.sum(self._pmf)
            #print("New PMF sum value:", np.sum(self._pmf))

    def pmf(self):
        return self._pmf

    def draw(self):
        return np.random.choice(np.arange(1, self.N+1), p=self._pmf)


# ---------- Generation of the model ----------

def __generate_uniform_transition_matrix(n_devices):
    return np.full((n_devices, n_devices), 1/n_devices)

SEQUENCE_LENGTH = 100


def __generate_transition_matrix_from_sequence(n_devices, sequence_length=SEQUENCE_LENGTH):
    devices_proba = get_devices_proba(n_devices)
    S = []
    connected = False

    while not connected:
        W = np.zeros((n_devices, n_devices), dtype=np.float)
        S = [np.random.choice(a=range(n_devices), p=devices_proba)
             for _ in range(sequence_length)]

        for i in range(sequence_length-1):
            W[S[i], S[i+1]] += 1

        if not is_connected(W):
            continue
        else:
            connected = True

        W = (W.T/W.sum(axis=1)).T
        W = np.nan_to_num(W)

    # print(S)

    return W

S_MIN = 1
S_MAX = 4


def __generate_zipf_transition_matrix(n_devices, s_min=S_MIN, s_max=S_MAX):
    W = np.zeros((n_devices, n_devices), dtype=np.float)

    for i in range(n_devices):
        zipf = zipf_gen(N=n_devices, s=random.uniform(s_min, s_max))
        W[i, :] = np.random.permutation(zipf.pmf())
        #print("Line {} has s={}".format(i, zipf.s))

    return W


def __generate_sparse_transition_matrix_from_zipf(n_devices, N=1000, s=1.8):
    # The bigger N, the bigger the potential gap b/w smallest an biggest probability
    # The bigger s, the least non-zero probabilities

    zipf = zipf_gen(N=N, s=s)
    connected = False

    while not connected:
        W = np.zeros((n_devices, n_devices), dtype=np.float)

        for i in range(n_devices):
            drawn = [zipf.draw()-1 for _ in range(n_devices)]
            while np.count_nonzero(drawn) == 0:
                drawn = [zipf.draw()-1 for _ in range(n_devices)]

            W[i, :] = drawn / np.sum(drawn)

        connected = is_connected(W)

    return W


def __generate_cyclic_transition_matrix(n_devices):
    W = np.zeros((n_devices, n_devices), dtype=np.float)

    for i in range(n_devices):
        j = (i+1) % n_devices
        W[i][j] = 1

    return W


def _generate_device_use_model(n_devices, model_type, **kwargs):
    if model_type == "uniform":
        return __generate_uniform_transition_matrix(n_devices)
    elif model_type == "from_sequence":
        return __generate_transition_matrix_from_sequence(n_devices, **kwargs)
    elif model_type == "zipf":
        return __generate_zipf_transition_matrix(n_devices, **kwargs)
    elif model_type == "sparse":
        return __generate_sparse_transition_matrix_from_zipf(n_devices, **kwargs)
    elif model_type == "cyclic":
        return __generate_cyclic_transition_matrix(n_devices)
    else:
        print("Invalid model type.")
        return None


def generate_device_use_model(n_devices, model_type, **kwargs):
    print("Generating a model of type '{}' with parameters: {}".format(
        model_type, str(kwargs)))

    W = _generate_device_use_model(n_devices, model_type, **kwargs)
    if W is None:
        return None

    while not is_connected(W):
        W = _generate_device_use_model(n_devices, model_type, **kwargs)

    return W
