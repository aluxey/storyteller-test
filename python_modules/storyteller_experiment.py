#!/usr/bin/env python3

from . import docker_utils
from .churn import DevicesScheduler
from .storyteller_sequence import generate_devices_sequence, \
    write_sequence_json
from .constants import PARAMETERS_FN, CHURN_FN, SEQUENCE_FN
import os
import pandas as pd
import subprocess as sp
import requests
import numpy as np
import sys
# import datetime
import time

# The following functions rely heavily on the experiment_parameters dictionary
# that contains every parameter of a given experiment
# e.g.
'''
experiment_parameters=dict(
    project_name=os.path.basename(os.getcwd()),
    experiment_name="first",
    net=dict(
        name="{project_name}_{experiment_id}",
        addr="13.{experiment_id}.0.0/20",
        gossip_port=10337,
        rest_port=10338, # Constant in main.go
        rest_url="/session"
    ),
    dirs=dict(
        root=os.getcwd()+"/",
        output_subdir="output/{experiment_name}/experiment{experiment_id}/",
        json_subdir="json/",
    ),
    build_script_path="dockerize_go_project.sh",

    image_name="{project_name}_image",
    container=dict(
        name="{project_name}_exp{experiment_id}_{container_id}",
        label="experiment{experiment_id}",
        ip="13.{experiment_id}.0.{container_id}",
        only_stop=False,
        arguments="-lcsv={log_to_csv} -debug={debug} \
-fanout={fanout} -csvdir={output_dir} -seq_path={seq_path} \
-net={net_addr} -id={device_id}"
    ),

    n_devices=12,
    n_experiments=10,

    # User behavioral model
    model=dict(
        type="sparse",
        parameters=dict(),
    ),

    # Churn scheduler
    churn=dict(
        enable=True,
    ),

    # Used but useless
    payload_size=1000,

    sequence=dict(
        initial_length=20,
        total_length=100,
    ),
    requests_interval=1,# s


    program_parameters=dict(
        log_to_csv=True, # Output CSV?
        debug=False, # Print debug info to stdout?
        fanout=4, # s
    )
)
'''

# ---------- Global variables ----------
# I'm not afraid of concurrent access!!!

# provide requests settings once and for all here
req_session = requests.Session()
req_session.headers.update({
    "Content-Type": "text/plain",
    # Not sure this is useful
    #"Access-Control-Allow-Origin": "*"
})
scheduler = None

# ---------- The Experiment ----------


def conduct(experiment_id, experiment_parameters):
    global scheduler, req_session
    # Create the output directory
    output_dir = experiment_parameters['dirs']['root']
    output_dir += experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'],
        experiment_id=experiment_id)
    try:
        os.makedirs(output_dir)
    except OSError:
        print("[conduct] {} exists: "
              "removing its content...".format(
                  output_dir))
        # Remove existing files if the dir exists
        for f in os.listdir(output_dir):
            os.remove(output_dir+f)
        pass

    print("Writing experiment parameters to", output_dir+PARAMETERS_FN)
    parameters_df = pd.DataFrame()
    parameters_df = parameters_df.append(pd.Series(dict(
        experiment_id=experiment_id,
        initial_sequence_length=experiment_parameters[
            'sequence']['initial_length'],
        total_sequence_length=experiment_parameters[
            'sequence']['total_length'],
        n_devices=experiment_parameters['n_devices'],
        model_type=experiment_parameters['model']['type'],
        model_parameters=experiment_parameters['model']['parameters'],
        requests_interval=experiment_parameters['requests_interval'],
        ** experiment_parameters['program_parameters']
    )), ignore_index=True)
    parameters_df.to_csv(output_dir+PARAMETERS_FN, index=False)

    devices, W, S, past_times = bootstrap(
        experiment_id, experiment_parameters)

    # Fill the sequence_df
    sequence_df = pd.DataFrame(columns=['Name', 'Address', "Timestamp"])
    for device_id, t in zip(S, past_times):
        sequence_df = sequence_df.append(pd.Series({
            'Name': devices[device_id]['name'],
            'Address': devices[device_id]['ip'],
            "Timestamp": t}), ignore_index=True)

    # Call the devices in sequence order
    initial_sequence_length = experiment_parameters[
        'sequence']['initial_length']
    for i, device_id in enumerate(S[initial_sequence_length:]):
        device = devices[device_id]
        url = "http://"+device['ip']
        url += ":"+str(experiment_parameters['net']['rest_port']
                       )+experiment_parameters['net']['rest_url']
        payload = np.random.bytes(experiment_parameters['payload_size'])

        # Rest request to the container
        scheduler.use(device_id)  # Will unpause the device if need be
        try:
            r = req_session.put(url, data=payload)
            if(r.status_code != 200):
                print("Error returned: status_code={}, answer={}".format(
                    r.status_code, r.text))
        except requests.ConnectionError as e:
            print("ConnectionError:\n\treq={}\n\tresp={}\n\terr={}".format(
                e.request, e.response, sys.exc_info()[1]))
            continue

        request_datetime = pd.to_datetime(r.json()['Timestamp'])

        sequence_df = sequence_df.append(pd.Series({
            'Name': device['name'],
            'Address': device['ip'],
            "Timestamp": request_datetime}), ignore_index=True)

        sleep_time = experiment_parameters['requests_interval']

        print(
            "[Experiment #{} session {}/{}] ({}) Used device {} (@ {}), "
            "now sleeping {:.2f}s".format(
                experiment_id, initial_sequence_length+i+1, len(S),
                request_datetime, device['name'], device['ip'], sleep_time))

        time.sleep(sleep_time)

    docker_utils.remove_containers(
        image_name=experiment_parameters['image_name'].format(
            project_name=experiment_parameters['project_name']),
        container_label=experiment_parameters['container'][
            'label'].format(experiment_id=experiment_id),
        only_stop=experiment_parameters['container']['only_stop'])

    print("[Experiment #{}] Waiting for scheduler to join...".format(
        experiment_id))
    # Churn CSVs are written here
    scheduler.kill()

    print("[Experiment #{}] Writing sequence to {}...".format(
        experiment_id, output_dir+SEQUENCE_FN))
    sequence_df.to_csv(output_dir+SEQUENCE_FN, index=False)

    docker_utils.remove_network(experiment_parameters['net'][
        'name'].format(
            project_name=experiment_parameters['project_name'],
            experiment_id=experiment_id))

    print("[Experiment #{}] Done!\n\n".format(experiment_id))


def bootstrap(experiment_id, experiment_parameters):
    global scheduler
    print("[Experiment #{}] Bootstrapping containers...".format(experiment_id))

    # TODO: ipadress.ip_network.hosts() would solve the 253 limitation
    if experiment_parameters['n_devices'] > 253:
        print("n_devices is too large, please make it below 253"
              " so it fits in /24.")
        return

    # Setting up Docker structures
    network = dict(
        name=experiment_parameters['net'][
            'name'].format(
                project_name=experiment_parameters['project_name'],
                experiment_id=experiment_id),
        addr=experiment_parameters['net'][
            'addr'].format(experiment_id=experiment_id)
    )
    output_dir = experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'],
        experiment_id=experiment_id)
    volumes = {
        'output': {
            'host_dir': experiment_parameters['dirs']['root']+output_dir,
            'cont_dir': "/"+output_dir,
            'mode': 'rw'
        },
        'json': {
            'host_dir':
                experiment_parameters['dirs']['root'] +
                experiment_parameters['dirs']['json_subdir'],
            'cont_dir': "/"+experiment_parameters['dirs']['json_subdir'],
            'mode': 'ro'
        },
    }
    label = experiment_parameters['container'][
        'label'].format(experiment_id=experiment_id)
    image_name = experiment_parameters['image_name'].format(
        project_name=experiment_parameters['project_name'])

    # Removing containers from previous experiment if they exist
    docker_utils.remove_containers(
        image_name=image_name,
        container_label=label,
        only_stop=False)
    # Creating the network
    docker_utils.remove_network(experiment_parameters['net'][
        'name'].format(
            project_name=experiment_parameters['project_name'],
            experiment_id=experiment_id))
    docker_utils.create_network(network)

    # We predefine the different devices identifiers
    devices_info = [None]*experiment_parameters['n_devices']
    for d_id in range(experiment_parameters['n_devices']):
        devices_info[d_id] = dict(
            name=experiment_parameters['container']['name'].format(
                project_name=experiment_parameters['project_name'],
                experiment_id=experiment_id,
                container_id=d_id),
            ip=experiment_parameters['container']['ip'].format(
                experiment_id=experiment_id,
                container_id=d_id+2))

    # Generating model and sequence
    W, S = generate_devices_sequence(experiment_parameters)

    # Writing the initial sequence to JSON
    json_fn = "experiment{}.json".format(experiment_id)
    past_times = write_sequence_json(
        S[:experiment_parameters['sequence']['initial_length']],
        devices_info, volumes['json']['host_dir'], json_fn)

    # Start the containers
    for d_id in range(experiment_parameters['n_devices']):
        cont_args = experiment_parameters['container']['arguments'].format(
            net_addr=network['addr'],
            output_dir=volumes['output']['cont_dir'],
            seq_path=volumes['json']['cont_dir']+json_fn,
            device_id=devices_info[d_id]['name'],
            **experiment_parameters['program_parameters'])

        cont_id, cont_ip = docker_utils.start_container(
            devices_info[d_id]['name'], cont_args, [label], network,
            image_name, volumes,
            cont_ip=devices_info[d_id]['ip'])

        # Add the docker container's ID to the devices info
        devices_info[d_id]['docker_id'] = cont_id

        print(
            "[Experiment #{} bootstrap {}/{}] "
            "Started container {} with Docker ID {} and IP {}".format(
                experiment_id, d_id+1, experiment_parameters['n_devices'],
                devices_info[d_id]['name'], cont_id, cont_ip))

    scheduler = DevicesScheduler(
        experiment_parameters.get('churn'),
        devices_info,
        experiment_parameters['dirs']['root']+output_dir)

    print("[Experiment #{}] Finished containers bootstrap.\n".format(
        experiment_id))

    scheduler.start()

    return devices_info, W, S, past_times


def build_project(experiment_parameters):
    try:
        sp.check_call([experiment_parameters['dirs']['root'] +
                       experiment_parameters['build_script_path'],
                       experiment_parameters['project_name']])
    except sp.CalledProcessError as e:
        print("Failed building project: error {}.\n\n".format(e.returncode))
        raise
    else:
        print("Successfully built project.")
