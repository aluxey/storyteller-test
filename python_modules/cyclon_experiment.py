#!/usr/bin/env python3

from . import docker_utils
from .constants import PARAMETERS_FN
import os
import pandas as pd
import subprocess as sp
import time

# The following functions rely heavily on the experiment_parameters dictionary
# that contains every parameter of a given experiment
# e.g.
'''
experiment_parameters=dict(
    project_name=os.path.basename(os.getcwd()),
    experiment_name="first",
    net=dict(
        name="{project_name}_{experiment_id}",
        addr="13.{experiment_id}.0.0/20",
        gossip_port=10337,
    ),
    dirs=dict(
        root=os.getcwd()+"/",
        output_subdir="output/{experiment_name}/experiment{experiment_id}/"
    ),
    build_script_path="dockerize_go_project.sh",

    image_name="{project_name}_image",
    container=dict(
        name="{project_name}_exp{experiment_id}_{container_id}",
        label="experiment{experiment_id}",
        ip="13.{experiment_id}.0.{container_id}",
        only_stop=False,
        arguments="-lcy={log_cy} -dcy={debug_cy} -cyperiod={period} \
-cygs={gossip_size} -cyvs={view_size} -bsip={bootstrap_ip} -csvdir={output_dir} \
-net={net_addr}"
    ),

    n_devices=100,
    n_experiments=10,
    experiment_duration=60,

    program_parameters=dict(
        log_cy=True, # Output CSV?
        debug_cy=False, # Print debug info to stdout?
        period=1, #s
        gossip_size=5,
        view_size=20,
    )
)
'''

# ---------- The Experiment ----------


def conduct(experiment_id, experiment_parameters):
    # Create the output directory
    output_dir = experiment_parameters['dirs']['root']
    output_dir += experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'],
        experiment_id=experiment_id)
    try:
        os.makedirs(output_dir)
    except OSError:
        print("[conduct] {} exists: "
              "removing its content...".format(
                  output_dir))
        # Remove existing files if the dir exists
        for f in os.listdir(output_dir):
            os.remove(output_dir+f)
        pass

    print("Writing experiment parameters to", output_dir+PARAMETERS_FN)
    parameters_df = pd.DataFrame()
    parameters_df = parameters_df.append(pd.Series(dict(
        experiment_id=experiment_id,
        n_devices=experiment_parameters['n_devices'],
        experiment_duration=experiment_parameters['experiment_duration'],
        ** experiment_parameters['program_parameters']
    )), ignore_index=True)
    parameters_df.to_csv(output_dir+PARAMETERS_FN, index=False)

    bootstrap(experiment_id, experiment_parameters)

    print("[Experiment #{}] Sleeping {}s".format(
        experiment_id, experiment_parameters['experiment_duration']))
    time.sleep(experiment_parameters['experiment_duration'])

    print("[Experiment #{}] Done, removing containers...".format(
        experiment_id))
    docker_utils.remove_containers(
        image_name=experiment_parameters['image_name'].format(
            project_name=experiment_parameters['project_name']),
        container_label=experiment_parameters['container'][
            'label'].format(experiment_id=experiment_id),
        only_stop=experiment_parameters['container']['only_stop'])
    docker_utils.remove_network(experiment_parameters['net'][
        'name'].format(
            project_name=experiment_parameters['project_name'],
            experiment_id=experiment_id))

    print("[Experiment #{}] Done!\n\n".format(experiment_id))


def bootstrap(experiment_id, experiment_parameters):
    print("[Experiment #{}] Bootstrapping containers...".format(experiment_id))

    # TODO: ipadress.ip_network.hosts() would solve the 253 limitation
    if experiment_parameters['n_devices'] > 253:
        print("n_devices is too large, please make it below 253"
              " so it fits in /24.")
        return

    # Setting up Docker structures
    network = dict(
        name=experiment_parameters['net'][
            'name'].format(
                project_name=experiment_parameters['project_name'],
                experiment_id=experiment_id),
        addr=experiment_parameters['net'][
            'addr'].format(experiment_id=experiment_id)
    )
    output_dir = experiment_parameters['dirs']['output_subdir'].format(
        experiment_name=experiment_parameters['experiment_name'],
        experiment_id=experiment_id)
    volumes = {
        'output': {
            'host_dir': experiment_parameters['dirs']['root']+output_dir,
            'cont_dir': "/"+output_dir,
            'mode': 'rw'
        }
    }
    label = experiment_parameters['container'][
        'label'].format(experiment_id=experiment_id)
    image_name = experiment_parameters['image_name'].format(
        project_name=experiment_parameters['project_name'])

    # Removing containers from previous experiment if they exist
    docker_utils.remove_containers(
        image_name=image_name,
        container_label=label,
        only_stop=False)
    # Creating the network
    docker_utils.remove_network(experiment_parameters['net'][
        'name'].format(
            project_name=experiment_parameters['project_name'],
            experiment_id=experiment_id))
    docker_utils.create_network(network)

    # We predefine the different devices identifiers
    bs_ip = ""
    devices = [None]*experiment_parameters['n_devices']
    for d_id in range(experiment_parameters['n_devices']):
        devices[d_id] = dict()
        devices[d_id]['name'] = experiment_parameters[
            'container']['name'].format(
            project_name=experiment_parameters['project_name'],
            experiment_id=experiment_id,
            container_id=d_id)
        # TODO: ipadress.ip_network.hosts() would solve the 253 limitation
        devices[d_id]['ip'] = experiment_parameters[
            'container']['ip'].format(
            experiment_id=experiment_id,
            container_id=d_id+2)

    # Start the containers
    for d_id in range(experiment_parameters['n_devices']):
        cont_args = experiment_parameters['container']['arguments'].format(
            net_addr=network['addr'],
            output_dir=volumes['output']['cont_dir'],
            bootstrap_ip=bs_ip,
            **experiment_parameters['program_parameters'])

        cont_id, cont_ip = docker_utils.start_container(
            devices[d_id]['name'], cont_args, [label], network,
            image_name, volumes,
            cont_ip=devices[d_id]['ip'])

        # Add the docker container's ID to the devices info
        devices[d_id]['docker_id'] = cont_id

        # We use the first IP as a bootstrap address for the next containers
        if d_id == 0:
            bs_ip = devices[d_id]['ip']

        print(
            "[Experiment #{} bootstrap {}/{}] "
            "Started container {} with Docker ID {} and IP {}".format(
                experiment_id, d_id+1, experiment_parameters['n_devices'],
                devices[d_id]['name'], cont_id, cont_ip))

    print("[Experiment #{}] Finished containers bootstrap.".format(
        experiment_id))

    return devices


def build_project(experiment_parameters):
    try:
        sp.check_call([experiment_parameters['dirs']['root'] +
                       experiment_parameters['build_script_path'],
                       experiment_parameters['project_name']])
    except sp.CalledProcessError as e:
        print("Failed building project: error {}.\n\n".format(e.returncode))
        raise
    else:
        print("Successfully built project.")
