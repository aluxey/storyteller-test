#!/usr/bin/env python3

from . import constants
import pandas as pd
import os
#import numpy as np

TIMESTAMP = "timestamp"
ID = "id"
SEQ_TIMESTAMPS = "sequenceTimestamps"


# experiments_path: path to experiments output (don't forget trailing slash)
def fetch_results(experiments_path):
    churn_df = pd.DataFrame()
    parameters_df = pd.DataFrame()
    sequence_df = pd.DataFrame()
    st_df = pd.DataFrame()

    subdirs = os.listdir(experiments_path)
    for subdir in subdirs:
        experiment_path = experiments_path+subdir+"/"

        # Get the parameters of this experiment
        these_parameters = pd.read_csv(experiment_path+constants.PARAMETERS_FN)
        these_parameters['path'] = experiment_path
        parameters_df = parameters_df.append(
            these_parameters, ignore_index=True)

        # Get the sequence
        this_sequence = pd.read_csv(experiment_path+constants.SEQUENCE_FN)
        this_sequence['experiment_id'] = int(these_parameters['experiment_id'])
        sequence_df = sequence_df.append(
            this_sequence,
            ignore_index=True)

        # And the devices CSVs
        for entry in os.scandir(experiment_path):
            if entry.name.startswith(constants.ST_FILE_EXPRESSION):
                # StoryTeller's CSV
                df = pd.read_csv(experiment_path+entry.name,
                                 parse_dates=[TIMESTAMP])
                df['experiment_id'] = int(these_parameters['experiment_id'])
                st_df = st_df.append(df, ignore_index=True)
            if entry.name.startswith(constants.CHURN_FILE_EXPRESSION):
                # CHURN CSV
                df = pd.read_csv(experiment_path+entry.name,
                                 parse_dates=[TIMESTAMP])
                df['experiment_id'] = int(these_parameters['experiment_id'])
                churn_df = churn_df.append(df, ignore_index=True)

    parameters_df.sort_values('experiment_id', inplace=True)

    st_df[SEQ_TIMESTAMPS] = st_df[SEQ_TIMESTAMPS].apply(splitList)
    st_df.sort_values(TIMESTAMP, inplace=True)

    churn_df.sort_values(TIMESTAMP, inplace=True)

    return churn_df, parameters_df, sequence_df, st_df


def splitList(x):
    if pd.isnull(x) or x == '' or x == 'nan':
        return []
    return x.split('|')
