#!/usr/bin/env python3

from . import model
import datetime
import os
import json

def generate_devices_sequence(experiment_parameters):
    # The model
    W = model.generate_device_use_model(
        experiment_parameters['n_devices'],
        experiment_parameters['model']['type'],
        **experiment_parameters['model']['parameters'])
    S = model.random_walk(W, experiment_parameters['sequence']['total_length'])

    return W, S

def write_sequence_json(S_init, devices, folder, fn):
    initial_sequence_length = len(S_init)
    serializable_S = [None]*initial_sequence_length
    past_times = [None]*initial_sequence_length
    now = datetime.datetime.utcnow()

    for i, s in enumerate(S_init):
        # We craft fake timestamps in increasing order
        s_time = now - datetime.timedelta(seconds=initial_sequence_length - i)

        it = dict(
            IP=devices[s]['ip'],
            ID=devices[s]['name'],
            Timestamp=s_time.isoformat()+'Z'
        )

        serializable_S[i] = it

        past_times[i] = s_time

    try:
        os.mkdir(folder)
    except:
        pass

    fd = open(folder+fn, 'w')
    json.dump(serializable_S, fd)
    print("Wrote initial sequence to", folder+fn)

    return past_times