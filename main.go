package main

import (
	"flag"
	"github.com/julienschmidt/httprouter"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/gossip/storyteller"
	"gitlab.inria.fr/aluxey/transport"
	"io"
	"log"
	"fmt"
	"bytes"
	"math/rand"
	"net/http"
	"time"
)

const (
	HTTP_PORT         = "10338"
	SESSION_REST_PATH = "/session"
)

var router *httprouter.Router

func init() {
	// rand is deterministic until you seed it
	rand.Seed(time.Now().UnixNano())
}

func main() {
	var err error

	// Parse command line parameters
	var logToCsv, debug bool
	var fanout int
	var csvDir, myID, netAddr, seqPath string
	flag.BoolVar(&logToCsv, "lcsv", false,
		"Log StoryTeller data to CSV?")
	flag.BoolVar(&debug, "debug", false,
		"Print debug information?")
	flag.IntVar(&fanout, "fanout", 1,
		"StoryTeller's fanout")
	flag.StringVar(&csvDir, "csvdir", "",
		"Directory where to save CSV output")
	flag.StringVar(&myID, "id", "",
		"Optional Device ID: a random one will be created if not provided")
	flag.StringVar(&netAddr, "net", "",
		"Address of the network in CIDR format (x.y.z.t/m)")
	flag.StringVar(&seqPath, "seq_path", "",
		"Location of the initial sequence")
	flag.Parse()

	// Set the CSV output location
	if err = csvlog.SetOutputLocation(csvDir); err != nil {
		log.Fatalf("Could not create the CSV output directory: %v\n", err)
	}

	// Create a descriptor for myself
	myIP := ""
	if myIP, err = transport.GetMyIP(netAddr); err != nil {
		log.Fatalf("Error getting my address: %v\n", err)
	}
	
	var myself gossip.Describer
	if myID == "" {
		myself = gossip.NewDescriptor(myIP)
	} else {
		myself = &gossip.Descriptor{MyIP: myIP, MyID: myID}
	}
	log.Printf("I am: %+v\n\n", myself)

	// Create a network manager
	var netManager transport.Manager
	if netManager, err = transport.NewTCPManager(myIP); err != nil {
		log.Fatalf("Error initialising TCP network manager: %v\n", err)
	}

	var st *storyteller.StoryTeller
	if st, err = storyteller.New(myself, netManager, fanout, seqPath,
		logToCsv, debug); err != nil {
		log.Fatalf("StoryTeller creation failed: %v\n", err)
	}

	// Launch the rest router
	go restRouterSetup(myself, st)

	// Start accepting gossip; will play forever
	st.ReceiveDaemon()
}

func restRouterSetup(myself gossip.Describer, st *storyteller.StoryTeller) {
	// We use "github.com/julienschmidt/httprouter" to create our REST API
	router = httprouter.New()

	router.PUT(SESSION_REST_PATH,
		func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
			// We answer JSON
			w.Header().Set("Content-Type", "application/json")
			// Allow cross domain JS requests (not sure it's needed)
			//w.Header().Set("Access-Control-Allow-Origin", "*")

			fmt.Printf("[restRouterSetup] Receiving a PUT request.\n")

			// Make a pointer to avoid copying content
			// Here, we don't care about the content, but anyway
			var payload bytes.Buffer
			if _, err := io.Copy(&payload, r.Body); err != nil {
				fmt.Fprintf(w,
					"{\"error\": \"I failed retrieving the session, fwiend.\", "+
						"\"error_content\": %q}\n", err)
			} else {
				it := storyteller.Interaction{
					D: myself.Copy(),
					// Rounding deletes the monotonic part that causes bugs
					Timestamp: time.Now().Round(0),
				}

				go st.AddNewInteraction(it)

				w.WriteHeader(200)
				fmt.Fprintf(w,
					"{\"info\": \"I updated my session, buddy.\", "+
						"\"Timestamp\": \"%v\", \"ID\": \"%v\", "+
						"\"IP\":\"%v\"}\n",
					it.Timestamp.Format(gossip.TIME_LAYOUT),
					myself.ID(), myself.IP())
			}
		})

	fmt.Printf("[restRouterSetup] REST Router ready for ignition.\n")
	// Blocking function
	http.ListenAndServe(":"+HTTP_PORT, router)
}
