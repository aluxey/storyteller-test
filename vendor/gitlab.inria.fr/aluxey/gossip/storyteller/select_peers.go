package storyteller

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"math/rand"
)

// TODO: doesn't belong here
// (storyteller does not care how the sequence is used by others)

// Roulette-wheel selection via stochastic acceptance (cf https://arxiv.org/abs/1109.3627)
// Does not need myself in exclude
func selectRoulettePeers(S *Sequence, fanout int,
	/*checkDevicesState bool, devicesStatePath string,*/
	sourceDevice gossip.Describer,
	exclude ...gossip.Describer) []gossip.Describer {

	//fmt.Printf("[SelectRoulettePeers] SourceDevice: %v\n", sourceDevice)

	// peersScore contains one entry per ONLINE device that was ALREADY USED
	peersScore, nOffline := getPeersScore(S,
		/*checkDevicesState, devicesStatePath,*/
		sourceDevice, exclude...)

	//fmt.Printf("[SelectRoulettePeers] PeersScore:\n%v\n", peersScore)

	selectedPeers := make([]gossip.Describer, 0)

	if len(peersScore) == 0 && nOffline > 0 {
		// I peersScore is too small (less that two), return random peers
		ret := selectRandomPeers(
			S, fanout, true,
			/*checkDevicesState, devicesStatePath, */
			exclude...)
		//fmt.Printf("[SelectRoulettePeers] Returning following RANDOM peers:\n%v\n\n", ret)
		return ret
		return []gossip.Describer{}
	} else if len(peersScore) <= fanout {
		for p, _ := range peersScore {
			selectedPeers = append(selectedPeers, p)
		}
		return selectedPeers
	}

	for len(selectedPeers) < fanout {
		var p gossip.Describer

		// Getting the maximum score
		maxScore := 0
		for _, score := range peersScore {
			if maxScore < score {
				maxScore = score
			}
		}

		accepted := false
		// We randomly pick a node p from peersScore, and accept it w/ proba of peersScore[p]/maxScore
		for !accepted {
			// Crappy uniform random selection in a map:
			// In the end, p contains a random peer from the map
			randomIndex := rand.Intn(len(peersScore))
			for p, _ = range peersScore {
				if randomIndex == 0 {
					break
				}
				randomIndex--
			}

			// Random draw to accept p as selected with a proba of peersScore[p]/maxScore
			accepted = rand.Float32() <= float32(peersScore[p])/float32(maxScore)
		}

		selectedPeers = append(selectedPeers, p)
		// We only accept each peer once: delete p from peersScore (and we will recompute maxScore above)
		delete(peersScore, p)
	}

	fmt.Printf("[SelectRoulettePeers] Returning following peers:\n%v\n\n", selectedPeers)

	return selectedPeers
}

func selectRandomPeers(S *Sequence, fanout int, debug bool,
	/*checkDevicesState bool, devicesStatePath string,*/
	exclude ...gossip.Describer) []gossip.Describer {

	descs := S.GetDescriptors(exclude...)

	// We choose 'fanout' random peers from
	if fanout > len(descs) {
		fanout = len(descs)
	}
	//ids := rand.Perm(len(descs))[:fanout]

	if debug {
		fmt.Printf("[SelectRandomPeers] Available gossip.Describers:\n%v\n", descs)
	}

	// And fill peers with the descs with the picked ids
	//peers := make([]gossip.Describer, fanout)
	peers := make([]gossip.Describer, 0)
	for _, x := range rand.Perm(len(descs)) {
		// Skip offline peers if checkDevicesState
		// if checkDevicesState &&
		// 	!isOnlinePeer(devicesStatePath, descs[x].ID()) {
		// 	continue
		// }

		peers = append(peers, descs[x])

		if len(peers) == fanout {
			break
		}
	}

	if debug {
		fmt.Printf("[SelectRandomPeers] Selected peers:\n%v\n", peers)
	}

	return peers
}

// ------------ Utils ------------

func getPeersScore(S *Sequence,
	/* checkDevicesState bool, devicesStatePath string,*/
	sourceDevice gossip.Describer,
	exclude ...gossip.Describer) (map[gossip.Describer]int, int) {

	// fmt.Printf("[SelectRoulettePeers] Source device:\n%v\n", sourceDevice)
	// fmt.Printf("[SelectRoulettePeers] Exclude set:\n%v\n", exclude)

	// Score: count of times each device was used right after our device
	peersScore := make(map[gossip.Describer]int)
	Skeys := S.GetSortedKeys()

	for i := 0; i < len(Skeys)-1; i++ {

		if S.GetInteraction(Skeys[i]).D.Equals(sourceDevice) &&
			!S.GetInteraction(Skeys[i+1]).D.Equals(sourceDevice) {

			// Skip gossip.Describers in exclude set
			if exclude != nil && isInArray(exclude, S.GetInteraction(Skeys[i+1]).D) {
				continue
			}

			// Skip offline peers if checkDevicesState
			// Now done below
			// if checkDevicesState &&
			// 	!isOnlinePeer(devicesStatePath, S.GetInteraction(Skeys[i+1]).D.ID()) {
			// 	continue
			// }

			if _, ok := peersScore[S.GetInteraction(Skeys[i+1]).D]; !ok {
				peersScore[S.GetInteraction(Skeys[i+1]).D] = 1
			} else {
				peersScore[S.GetInteraction(Skeys[i+1]).D] += 1
			}
		}
	}

	// TODO: better connection detection
	// // Skip offline peers if checkDevicesState
	nOffline := 0
	// if checkDevicesState {
	// 	for p := range peersScore {
	// 		if !isOnlinePeer(devicesStatePath, p.ID()) {
	// 			nOffline += 1
	// 			delete(peersScore, p)
	// 		}
	// 	}
	// }

	return peersScore, nOffline
}

// Little linear search function
func isInArray(arr []gossip.Describer, desc gossip.Describer) bool {
	for _, d := range arr {
		if d == nil {
			continue
		}
		if desc.Equals(d) {
			return true
		}
	}
	return false
}

// Check device's state in status file
// func isOnlinePeer(path, deviceID string) bool {
// 	if v, err := ioutil.ReadFile(path + deviceID); err != nil {
// 		fmt.Fprintf(os.Stderr, "[isOnlinePeer] Failed opening %s: %v.\n", path+deviceID, err)
// 		return false
// 	} else {
// 		return string(v[:]) == "1\n"
// 	}
// }
