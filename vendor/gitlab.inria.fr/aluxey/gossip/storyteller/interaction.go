package storyteller

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"time"
)

type Interaction struct {
	D         gossip.Describer `json:"D"`
	Timestamp time.Time        `json:"Timestamp"`
}

func (it Interaction) String() string {
	return fmt.Sprintf("Interaction{D: %v, Timestamp: %v}",
		it.D, it.Timestamp)
}
func (it Interaction) Equals(s2 Interaction) bool {
	return it.D.Equals(s2.D) && it.Timestamp.Equal(s2.Timestamp)
}
