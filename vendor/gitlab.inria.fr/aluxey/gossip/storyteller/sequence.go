package storyteller

import (
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"os"
	"sort"
	"time"
)

type Sequence map[time.Time]Interaction

func NewSequence(its ...Interaction) *Sequence {
	var S Sequence = make(map[time.Time]Interaction)
	S.Add(its...)
	return &S
}

func (S *Sequence) Add(its ...Interaction) {
	for _, it := range its {
		(*S)[it.Timestamp] = it
	}
}
func (S *Sequence) Copy() *Sequence {
	var S2 Sequence = make(map[time.Time]Interaction)
	for key, item := range *S {
		S2[key] = item
	}
	return &S2
}
func (S *Sequence) Equals(S2 *Sequence) bool {
	if len(*S) != len(*S2) {
		return false
	}
	for _, it := range *S {
		if !S2.Has(it) || !it.Equals(S2.GetInteraction(it.Timestamp)) {
			return false
		}
	}

	return true
}

// Unused?
// func (S *Sequence) Get(t time.Time) (Interaction, bool) {
// 	return (*S)[t]
// }

func (S *Sequence) GetDescriptors(
	exclude ...gossip.Describer) (descsArr []gossip.Describer) {
	// Little linear search function
	isInArray := func(arr []gossip.Describer, desc gossip.Describer) bool {
		for _, d := range arr {
			if d == nil {
				continue
			}
			if desc.Equals(d) {
				return true
			}
		}
		return false
	}

	for _, it := range *S {
		// Continue if it.D was already in the descsMap
		if isInArray(descsArr, it.D) {
			continue
		}

		// Add it.D in descsArr only if not in exclude
		if !isInArray(exclude, it.D) {
			descsArr = append(descsArr, it.D)
		}
	}

	return
}

func (S *Sequence) GetInteraction(t time.Time) Interaction {
	return (*S)[t]
}

func (S *Sequence) GetLastInteraction() Interaction {
	sortedKeys := S.GetSortedKeys()
	lastTimestamp := sortedKeys[len(sortedKeys)-1]

	return (*S)[lastTimestamp]
}

func (S *Sequence) GetSortedKeys() []time.Time {
	keys := make([]time.Time, len(*S))
	i := 0
	for k := range *S {
		keys[i] = k
		i++
	}

	sort.Sort(timeSlice(keys))
	return keys

}
func (S *Sequence) Has(it Interaction) bool {
	_, ok := (*S)[it.Timestamp]
	return ok
}
func (S *Sequence) Len() int {
	return len(*S)
}
func (S *Sequence) Merge(S2 *Sequence) bool {
	updated := false
	for k, v := range *S2 {
		if it, ok := (*S)[k]; ok && !it.Equals(v) {
			fmt.Fprintf(os.Stderr, "[Sequence.Merge] Sequence has different element at key %q:\n"+
				"\tIn S: %q; In S2: %q\n", k, it, v)
		} else if !ok {
			(*S)[k] = v
			updated = true
		}
	}

	return updated
}
func (S *Sequence) Remove(its ...Interaction) {
	for _, it := range its {
		delete(*S, it.Timestamp)
	}
}
func (S *Sequence) String() string {
	ret := "Sequence{\n"
	for _, t := range S.GetSortedKeys() {
		ret += fmt.Sprintf("%v\n", (*S)[t])
	}
	return ret + "}"
}

func SequenceDiff(S1, S2 *Sequence) *Sequence {
	S := S1.Copy()
	for key2 := range *S2 {
		delete(*S, key2)
	}

	return S
}

// Sort internal bullshit
type timeSlice []time.Time

func (t timeSlice) Len() int           { return len(t) }
func (t timeSlice) Less(i, j int) bool { return t[i].Before(t[j]) }
func (t timeSlice) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }
