package storyteller

import (
	"encoding/json"
	"fmt"
	"gitlab.inria.fr/aluxey/gossip"
	"io/ioutil"
	"os"
	"time"
)

type itJson struct {
	// D only contains the IP, not the port: we need to add it
	ID        string `json:"ID"`
	IP        string `json:"IP"`
	Timestamp string `json:"Timestamp"`
	// SessionHash [sha1.Size]byte
}

type sequenceJson []itJson

func loadInitialSequence(path string) (*Sequence, error) {
	sequence := NewSequence()

	if path == "" {
		return sequence, fmt.Errorf("[loadInitialSequence] Please provide a path.\n")
	}

	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return sequence, fmt.Errorf("[loadInitialSequence] Failed opening %v: %v\n",
			path, err)
	}

	var seq sequenceJson
	if err = json.Unmarshal(bytes, &seq); err != nil {
		return sequence, fmt.Errorf("[loadInitialSequence] Failed unmarshaling JSON file: %v\n",
			err)
	}

	for _, itJson := range seq {
		var t time.Time
		if t, err = time.Parse(gossip.TIME_LAYOUT, itJson.Timestamp); err != nil {
			fmt.Fprintf(os.Stderr, "[loadInitialSequence] Failed decoding time %q: %v\n",
				itJson.Timestamp, err)
			continue
		}

		sequence.Add(Interaction{
			D: &gossip.Descriptor{
				itJson.ID,
				itJson.IP},
			Timestamp: t,
		})
	}

	fmt.Printf("[loadInitialSequence] We successfully extracted the initial sequence:\n%v\n", sequence)

	return sequence, nil
}
