package storyteller

import (
	"encoding/gob"
	"fmt"
	"gitlab.inria.fr/aluxey/csvlog"
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	ACK_TIMEOUT = 300 // ms
	// defined in gossip
	//TIME_LAYOUT = time.RFC3339Nano
)

func init() {
	// gob.Register(&Interaction{})
	// gob.Register(&Sequence{})
	gob.Register(Payload{})
}

// TODO if needed: receiver pattern like in cyclon?

type StoryTeller struct {
	myself gossip.Describer

	// My sequence, and the initial sequence given on bootstrap
	sequence, initialSequence *Sequence
	// Our knowledge of our peers' sequences
	otherSequences struct {
		sync.RWMutex
		m map[string]*Sequence
	}

	// Number of peers to which we communicate our sequence updates
	fanout int

	// Network
	tm                  transport.Manager
	reqCh, ansCh, ackCh <-chan transport.Message

	// Acknowledgement map
	ackMap struct {
		sync.RWMutex
		m map[int32]chan struct{}
	}

	// Logging
	csv         *csvlog.CSVLog
	info, debug *log.Logger
	// count #messages and # useless messages received
	nMessages, nUselessMessages int

	mutex sync.RWMutex
}

func New(myself gossip.Describer, tm transport.Manager,
	fanout int, sequencePath string,
	logToCsv, debug bool) (*StoryTeller, error) {

	st := new(StoryTeller)

	st.fanout = fanout
	st.myself = myself

	// ---------- Sequences & Descriptor ----------
	var err error
	if st.initialSequence, err = loadInitialSequence(sequencePath); err != nil {
		return nil, err
	}
	st.sequence = st.initialSequence.Copy()

	// Initialise the otherSequences map:
	// for each descriptor in our S, we consider that each other node knows the same sequence
	st.otherSequences.m = make(map[string]*Sequence)
	for _, d := range st.sequence.GetDescriptors(st.myself) {
		st.otherSequences.m[d.ID()] = st.initialSequence.Copy()
	}

	// ---------- Network ----------
	st.tm = tm
	// Creation of the chans
	reqCh := make(chan transport.Message)
	ansCh := make(chan transport.Message)
	ackCh := make(chan transport.Message)
	// Register chans on the network Manager
	st.tm.RegisterObserver(transport.MessageType("ST_REQ"), reqCh)
	st.tm.RegisterObserver(transport.MessageType("ST_ANS"), ansCh)
	st.tm.RegisterObserver(transport.MessageType("ST_ACK"), ackCh)
	// Save receive chan in st object
	st.reqCh = reqCh
	st.ansCh = ansCh
	st.ackCh = ackCh

	// Acknowledgement map
	st.ackMap.m = make(map[int32]chan struct{})

	// ---------- Logging ----------
	// To console
	st.info = log.New(os.Stdout, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	if debug {
		st.debug = log.New(os.Stdout, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	} else {
		st.debug = log.New(ioutil.Discard, "STORYTELLER: ", log.Ltime|log.Lshortfile)
	}
	// To CSV
	if logToCsv {
		now := time.Now()
		filename := fmt.Sprintf("storyteller-%v_%02d-%02d-%02d_%02dh%02d.csv",
			st.myself.ID(),
			now.Day(), now.Month(), now.Year(), now.Hour(), now.Minute())
		header := []string{"id", "ip", "timestamp", "sequenceTimestamps",
			"updateSource", "nMessagesReceived",
			"nUselessMessagesReceived"}

		var err error
		if st.csv, err = csvlog.New(filename, header); err != nil {
			return nil, fmt.Errorf("Failed creating StoryTeller CSVLog: %v", err)
		}
	}

	st.info.Printf("StoryTeller initialized with fanout=%d.\n", st.fanout)

	return st, nil
}

func (st *StoryTeller) ReceiveDaemon() {
	st.info.Printf("ReceiveDaemon started.\n")

	for {
		select {
		case mess := <-st.reqCh:
			go st.handleRequest(mess)
		case mess := <-st.ansCh:
			go st.handleAnswer(mess)
		case mess := <-st.ackCh:
			go st.dispatchAck(mess)
		}
	}
}

func (st *StoryTeller) AddNewInteraction(it Interaction) {
	st.mutex.Lock()
	defer st.mutex.Unlock()

	st.sequence.Add(it)

	st.addLogLine(it.Timestamp, st.myself)

	st.info.Printf("User performed a new interaction:\n%v\n\n", it)

	go st.gossipUpdate(nil)
}

func (st *StoryTeller) Sequence() *Sequence {
	st.mutex.RLock()
	defer st.mutex.RUnlock()
	return st.sequence.Copy()
}

// Does not belong here
// func (st *StoryTeller) SelectPeers(fanout int, rouletteSel bool,
// 	sourceDevice gossip.Describer,
// 	exclude ...gossip.Describer) []gossip.Describer {

// 	st.mutex.RLock()
// 	S := st.sequence.Copy()
// 	myself := st.myself
// 	st.mutex.RUnlock()

// 	if sourceDevice == nil || !sourceDevice.Equals(myself) {
// 		if exclude == nil {
// 			exclude = []gossip.Describer{myself}
// 		} else {
// 			exclude = append(exclude, myself)
// 		}
// 	}

// 	if rouletteSel {
// 		return selectRoulettePeers(S, fanout,
// 			/*st.checkDevicesState, st.devicesStatePath,*/
// 			sourceDevice, exclude...)
// 	} else {
// 		return selectRandomPeers(S, fanout, false,
// 			/*st.checkDevicesState, st.devicesStatePath,*/
// 			exclude...)
// 	}
// }

func (st *StoryTeller) gossipUpdate(excluded gossip.Describer) {
	st.mutex.RLock()
	lastUsedDevice := st.sequence.GetLastInteraction().D
	sequence := st.sequence.Copy()
	st.mutex.RUnlock()

	// We will communicate with nodes from our sequence
	// excluding myself, the device used in the last interaction,
	// and the optional 'excluded' peer
	// peers := st.SelectPeers(st.fanout, false, nil,
	// 	st.myself, lastUsedDevice, excluded)
	peers := selectRandomPeers(sequence, st.fanout, false,
		st.myself, lastUsedDevice, excluded)

	//peers := SelectRandomPeers(st.sequence, st.fanout, st.myself, lastUsedDevice, excluded)
	st.info.Printf("We will gossip the new interaction with:\n%v\n",
		peers)

	for _, peer := range peers {
		st.otherSequences.Lock()
		if _, ok := st.otherSequences.m[peer.ID()]; !ok {
			// st.debug.Printf("First time talking to %v: adding it to the otherSequencesmap.\n",
			// 	peer)
			st.otherSequences.m[peer.ID()] = st.initialSequence.Copy()
		}

		// diffSeq contains the its we believe peer does not know of among our sequence:
		// diffSeq = our Sequence \ peer's local Sequence
		diffSeq := SequenceDiff(sequence, st.otherSequences.m[peer.ID()])
		st.otherSequences.Unlock()

		// We only communicate with peer if we have something for it
		if diffSeq.Len() > 0 {
			st.debug.Printf("Sending the following REQUEST to %v:\n%v\n\n",
				peer, diffSeq)

			// Message has a unique messageID
			req := Payload{st.myself, diffSeq, rand.Int31()}

			go transport.EncodeAndSend(
				req,
				peer.IP(),
				st.tm,
				transport.MessageType("ST_REQ"))

			// Add it to the ack map
			st.ackMap.Lock()
			ch := make(chan struct{})
			st.ackMap.m[req.ID] = ch
			st.ackMap.Unlock()

			select {
			// We have received the Acknowledgement
			case <-ch:
				// Update our local view of peer's sequence
				st.debug.Printf(
					"Ack from %v for our REQUEST received: merging views.\n",
					peer)
				st.otherSequences.Lock()
				st.otherSequences.m[peer.ID()].Merge(diffSeq)
				st.otherSequences.Unlock()
			// It timed out
			case <-time.After(ACK_TIMEOUT * time.Millisecond):
				st.debug.Printf("Ack from %v for our REQUEST timed out.\n",
					peer)
			}

		} else {
			st.debug.Printf("%v seems up to date with our sequence: skpping it.\n",
				peer)
		}
	}
}

func (st *StoryTeller) handleRequest(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_REQ"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	st.info.Printf("Received the following %s from %v:\n%v\n\n",
		mess.Type, payload.Sender, payload.S)

	// First sending acknowledgement to the remote
	// We set the payload Sequence to nil: no need
	go transport.EncodeAndSend(
		Payload{st.myself, nil, payload.ID},
		payload.Sender.IP(), st.tm, transport.MessageType("ST_ACK"))

	// Locking the otherSequences mutex while we use it
	st.otherSequences.Lock()
	if _, ok := st.otherSequences.m[payload.Sender.ID()]; !ok {
		st.otherSequences.m[payload.Sender.ID()] = st.initialSequence.Copy()
	}
	// We update our local version of the remote's sequence with what it sent
	st.otherSequences.m[payload.Sender.ID()].Merge(payload.S)

	itsSeq := st.otherSequences.m[payload.Sender.ID()].Copy()
	st.otherSequences.Unlock()

	// Locking the st Mutex while we modify/access the sequence
	st.mutex.Lock()

	// We update our sequence with the received payload
	updatedMySequence := st.sequence.Merge(payload.S)

	// Just a little logging
	if st.csv != nil {
		// Incrementing messages counters
		st.nMessages++
		if !updatedMySequence {
			st.nUselessMessages++
		} else {
			// If my sequence was updated, add a csv line
			go st.addLogLine(time.Now(), payload.Sender)
		}
	}

	// diffSeq contains the interactions known by us and not by the remote
	diffSeq := SequenceDiff(st.sequence, itsSeq)

	st.mutex.Unlock()

	// Only answer to the remote if we have interactions to send it
	if diffSeq.Len() > 0 {
		st.debug.Printf("Sending the following ANSWER to %v:\n%v\n\n",
			payload.Sender, diffSeq)

		ans := Payload{st.myself, diffSeq, rand.Int31()}

		go transport.EncodeAndSend(
			ans,
			payload.Sender.IP(),
			st.tm,
			transport.MessageType("ST_ANS"))

		st.ackMap.Lock()
		ch := make(chan struct{})
		st.ackMap.m[ans.ID] = ch
		st.ackMap.Unlock()

		select {
		// We have received the Acknowledgement
		case <-ch:
			// Update our local view of peer's sequence
			st.debug.Printf("Ack from %v for our ANSWER received: merging views.\n",
				payload.Sender)
			st.otherSequences.Lock()
			st.otherSequences.m[payload.Sender.ID()].Merge(diffSeq)
			st.otherSequences.Unlock()
		// It timed out
		case <-time.After(ACK_TIMEOUT * time.Millisecond):
			st.debug.Printf("Ack from %v for our ANSWER timed out.\n",
				payload.Sender)
		}

	} else {
		st.debug.Printf("Nothing to answer to %v, we're up to date!\n\n",
			payload.Sender)
	}

	// If my sequence was updated with this request, we forward the update to other peers
	// Excluding the remote that sent us the update
	if updatedMySequence {
		go st.gossipUpdate(payload.Sender)
	}
}

func (st *StoryTeller) handleAnswer(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_ANS"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	st.debug.Printf("Received the following %s from %v:\n%v\n\n",
		mess.Type, payload.Sender, payload.S)

	// First sending acknowledgement to the remote
	go transport.EncodeAndSend(
		Payload{st.myself, nil, payload.ID},
		payload.Sender.IP(), st.tm, transport.MessageType("ST_ACK"))

	// We update our local version of the remote's sequence with what it sent
	st.otherSequences.Lock()
	if _, ok := st.otherSequences.m[payload.Sender.ID()]; !ok {
		st.otherSequences.m[payload.Sender.ID()] = st.initialSequence.Copy()
	}
	st.otherSequences.m[payload.Sender.ID()].Merge(payload.S)
	st.otherSequences.Unlock()

	// Locking the mutex while we modify attributes
	st.mutex.Lock()
	defer st.mutex.Unlock()

	// We update our sequence with the received payload
	updatedMySequence := st.sequence.Merge(payload.S)

	if st.csv != nil {
		st.nMessages++
		if !updatedMySequence {
			st.nUselessMessages++
		} else {
			// If my sequence was updated and we log, add a csv line
			go st.addLogLine(time.Now(), payload.Sender)
		}
	}
}

func (st *StoryTeller) dispatchAck(mess transport.Message) {
	x := transport.Decode(mess, transport.MessageType("ST_ACK"))
	if x == nil {
		st.info.Printf(
			"Failed decoding payload.\n")
		return
	}
	payload := x.(Payload)

	st.ackMap.RLock()
	defer st.ackMap.RUnlock()
	if ch, ok := st.ackMap.m[payload.ID]; ok {
		// Closing the chan will unlock goroutines waiting on it
		close(ch)
	} else {
		st.info.Printf("Received unexpected ACK: %v.\n",
			payload)
	}
}

// Log a new line to the csvlog
func (st *StoryTeller) addLogLine(ts time.Time, updateSource gossip.Describer) {
	if st.csv != nil {

		nMessages, nUselessMessages := st.nMessages, st.nUselessMessages
		st.nMessages, st.nUselessMessages = 0, 0

		S := ""
		timestamps := st.sequence.GetSortedKeys()
		for i, k := range timestamps {
			S += k.Format(gossip.TIME_LAYOUT)
			if i != len(timestamps)-1 {
				S += gossip.ID_SEPARATOR
			}
		}

		// header: {"id", "ip", "timestamp", "sequenceTimestamps",
		// "updateSource", "nMessagesReceived",
		// "nUselessMessagesReceived"}

		st.csv.Write([]string{
			st.myself.ID(),
			st.myself.IP(),
			// Python recognizes RFC3339Nano well, and it's high-precision
			// For instance one can use pandas.to_datetime(...) to parse it
			ts.Format(gossip.TIME_LAYOUT),
			S,
			updateSource.ID(),
			strconv.Itoa(nMessages),        // number of received messages since last line
			strconv.Itoa(nUselessMessages), // number of received useless messages since last line
		})
	}
}
