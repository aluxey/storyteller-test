package storyteller

import (
	"gitlab.inria.fr/aluxey/gossip"
	"gitlab.inria.fr/aluxey/transport"
)

type Payload struct {
	Sender gossip.Describer
	S      *Sequence
	ID     int32
}

type ActivityPayload struct {
	Sender    gossip.Describer
	S         *Sequence
	MessageID int32
}

type AckPayload struct {
	Sender    gossip.Describer
	Type      transport.MessageType
	MessageID int32
}
