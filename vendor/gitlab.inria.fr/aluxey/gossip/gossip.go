package gossip

import (
	"encoding/gob"
	"time"
)

func init() {
	gob.Register(&Descriptor{})
	gob.Register(&View{})
}

const (
	// Python recognizes RFC3339Nano well, and it's high-precision
	// For instance one can use pandas.to_datetime(...) to parse it
	TIME_LAYOUT = time.RFC3339Nano
)

// Describer is a network node profile
// A Describer is identified by its ID
type Describer interface {
	Copy() Describer
	Equals(Describer) bool
	ID() string
	IP() string
	String() string
}

// Viewer is a set handling Describers
type Viewer interface {
	Add(items ...Describer) error
	Copy() Viewer
	Diff(Viewer)
	Empty() bool
	Equals(v2 Viewer) bool
	Has(item Describer) bool
	IDs() string
	Len() int
	List() []Describer         // Returns a copy
	Map() map[string]Describer // Returns a copy
	//Merge(views ...Viewer) error
	Remove(items ...Describer)
	String() string
}
