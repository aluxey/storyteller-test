# Gossip - A Go package for general gossip utilities

Gossip algorithms are a kind of distributed algorithms to share information
between a set of peers. 
It scales very well with the number of peers, because nodes only 
communicate with a bounded number of peers at a time. 

They often have the same building blocks: 

* Each node is represented by a `Descriptor` (of interface type `Describer`);

* Each nodes has a `View` (of interface type `Viewer`) that contains a number 
of other peers' descriptors;

* Some bootstrap mechanism (because nodes need a minimum information, such as 
a peer's address, to start gossiping). Here is implemented the `ListBootstrap` 
(of interface type `Bootstrapper`): it will provide IPs from a 
predefined list to the client algorithm.

This was the complete list of this Go package's content! :)
The subdirectories contain actual implementations that use `gossip`. 
Go check them out!

## Install

	go get gitlab.inria.fr/aluxey/gossip 

## Usage

* Read the code, understand it, and then use it;

* If frustrated, ping me at adrien.luxey at irisa.fr and I will write documentation.

----

By Adrien Luxey in June 2018.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.